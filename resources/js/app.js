window.axios = require('axios');
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}

import Vue from 'vue';
import PortalVue from 'portal-vue';
import Modals from '@mindofmicah/modal-component';

import Popper from 'popper.js/dist/umd/popper.js';

window.$ = window.jQuery = require('bootstrap');
window.Popper = Popper;

Vue.use(PortalVue);
Vue.use(Modals, {});

new Vue({
    el:'#app',
    data: {
        output:'',
        modal_open:false,
        verbosity:1,
        errors:[],
    },
    methods:{
        submit(evt) {
            this.modal_open = true;
            this.errors = [];
            this.output = '';

            const form_data = new FormData(evt.target);
            form_data.set('verbosity', this.verbosity);
            
            axios.post(evt.target.action, form_data)
            .then(({data}) => {
                this.output = data.output;
            }).catch(err => {
                if(err.response.status === 422) {
                    this.errors = err.response.data.errors.map(error => error[0]);
                    return;
                }
                this.errors.push('An Error Occurred');
            });
        }
    }
});
