<?php
return [
    'option-values' => [
        '*'=> [

        ],
    ],
    'favorite'=> [
        'commands'=> [
        ],
        'namespaces'=>[
            'favorites',
        ],  
    ],
    'excluded'=> [
        'list',
        'clear-compiled',
        'auth:clear-resets',
        'db:wipe',
        'env',
        'key:generate',
        'optimize',
        'optimize:clear',
        'package:discover',
        'preset',
        'serve',
        'tinker',
        'inspire',
    ],
    'route'=> [
        'prefix' => 'commands'
    ],
];
