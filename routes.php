<?php
Route::group([
    'prefix' => config('artisan-gui.route.prefix'),
    'as'     => 'artisan-gui.'
], function () {
    Route::get('/', '\MindOfMicah\ArtisanGUI\Http\Controllers\ArtisanController@index')->name('list');
    Route::post('/{command}', '\MindOfMicah\ArtisanGUI\Http\Controllers\ArtisanController@show')->name('run');
});
