<?php

namespace MindOfMicah\ArtisanGUI\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Contracts\Console\Kernel;
use Illuminate\Config\Repository as Config;
use MindOfMicah\ArtisanGUI\OptionValueCache;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ArtisanController extends Controller
{
    public function index(Kernel $artisan, Config $config, OptionValueCache $value_cache)
    {
        $excluded             = $config->get('artisan-gui.excluded');
        $preferred_namespaces = $config->get('artisan-gui.favorite.namespaces');
        $favorite_commands    = $config->get('artisan-gui.favorite.commands');

        $commands = collect($artisan->all())
            ->diffKeys(array_flip($excluded))
            ->map(function ($command, $command_name) use ($value_cache) {
                $definition = $command->getDefinition();
                [$switches, $options] = collect($definition->getOptions())->map(function (InputOption $option, $name) use ($value_cache, $command_name) {
                    return (object)[
                        'name'        => '--'. $name,
                        'label'       => $name,
                        'description' => $option->getDescription(),
                        'default'     => $option->isArray()
                            ? implode(',', $option->getDefault())
                            : $option->getDefault(),
                        'is_required' => $option->isValueRequired(),
                        'switch'      => !$option->acceptValue(),
                        'is_array'    => $option->isArray(),
                        'option'      => $option,
                        'values'      => $value_cache->get($command_name, $name),
                    ];
                })->partition('switch');
                $arguments = collect($definition->getArguments())->map(function (InputArgument $argument, $name) use ($value_cache, $command_name) {

                    return (object)[
                        'name'        => $name,
                        'label'       => $name,
                        'description' => $argument->getDescription(),
                        'default'     => $argument->isArray()
                            ? implode(',', $argument->getDefault())
                            : $argument->getDefault(),
                        'is_array'    => $argument->isArray(),
                        'is_required' => $argument->isRequired(),
                        'argument'    => $argument,
                        'values'      => $value_cache->get($command_name, $name),
                    ];
                });

                return (object)[
                    'name'        => $command_name,
                    'description' => $command->getDescription(),
                    'arguments'   => $arguments,
                    'switches'    => $switches,
                    'options'     => $options,
                    'has_options' => $options->count() || $arguments->count() || $switches->count(),
                ];
            })->groupBy(function ($command, $name) use ($favorite_commands) {
                if (in_array($name, $favorite_commands)) {
                    return 'favorites';
                }

                return explode(':', $name)[0];
            })->pipe(function ($commands) {
                [$misc, $commands] = $commands->partition(function ($commands) {
                    return $commands->count() === 1 && (strpos($commands->first()->name, ':') === false);
                });

                return $commands->put('Miscellaneous', $misc->flatten());
            })->sortBy(function ($commands, $namespace) use ($preferred_namespaces) {
                $prefix = array_search(strtolower($namespace), $preferred_namespaces);
                if ($prefix !== false) {
                    return "$prefix-$namespace";
                }

                return $namespace;
            });

        return view('artisan-gui::main', compact('commands'));
    }

    public function show(string $command, \Illuminate\Http\Request $request)
    {
        $args = array_merge(
            $request->arguments ?? [],
            $request->options ?? [],
            array_fill_keys(array_keys($request->switches ?? []), true)
        );

        foreach ($request->get('arrays', []) as $key) {
            if (isset($args[$key])) {
                $args[$key] = [$args[$key]];
            }
        }

        if ($request->verbosity) {
            $args['--verbose'] = $request->verbosity;
        }

        try {
            \Artisan::call($command, $args);
        } catch (\Exception $e) {
            throw \Illuminate\Validation\ValidationException::withMessages([$e->getMessage()]);
        }

        return response()->json(['output' => $this->formatOutput(\Artisan::output())]);
    }


    private function formatOutput(string $output): string
    {
        $table_border_pattern = '/^(\+(?:\-+\+)+)$/m';
        if (preg_match_all($table_border_pattern, $output, $matches)) {
            foreach (array_unique($matches[1]) as $line_pattern) {
                $chunks = explode($line_pattern, $output);
                $output = array_shift($chunks) . '<div class="table-responsive"><table class="table">';
                $suffix = array_pop($chunks);

                if (count($chunks) === 2) {
                    $output .= $this->formatTableHeaders(array_shift($chunks));
                }
                $output .= $this->formatTableRows(trim($chunks[0]));
                $output .= '</table></div>' . $suffix;
            }
        }

        return $output;
    }

    private function formatTableHeaders($output)
    {
        $headers = (array_map('trim', explode(' | ', trim($output, "\n| "))));

        return '<thead><tr><th>' . implode('</th><th>', $headers) . '</th></tr></thead>';
    }

    private function formatTableRows($output)
    {
        $rows = array_map(function (string $line) {
            return '<td>' . implode('</td><td>', (array_map('trim', explode(' | ', trim($line, "\n|"))))) . '</td>';
        }, explode("\n", $output));

        return '<tbody><tr>' . implode('</tr><tr>', $rows) . '</tr></tbody>';
    }
}