<?php
namespace MindOfMicah\ArtisanGUI\Contracts;

interface CommandOptionable
{
    public function values(): array;
}