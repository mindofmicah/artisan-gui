<?php
namespace MindOfMicah\ArtisanGUI;

use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider 
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'artisan-gui');
        $this->loadRoutesFrom(__DIR__.'/../routes.php');

        $this->publishes([
            __DIR__. '/../config.php' => config_path('artisan-gui.php'),
            __DIR__.'/../public'      => public_path('vendor/artisan-gui'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config.php', 'artisan-gui');

        $this->app->singleton(OptionValueCache::class, function () {
            $value_config = $this->app['config']['artisan-gui']['option-values'];
            $globals = Arr::pull($value_config, '*');

            $cache = new OptionValueCache;
            $cache->prepare($value_config, $globals);

            return $cache;
        });
    }
}
