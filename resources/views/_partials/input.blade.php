<div class="md:w-1/3 sm:w-1/2 w-full sm:px-2 form-group">
    <label>{{$field->label}}</label>
    @if($field->values)
        {{html()->select($key .'['.$field->name.']', $field->values)->class('form-control')->required($field->is_required && !$field->default)}}
    @else
        {{html()->text($key .'['.$field->name.']', $field->default)->class('form-control')->required($field->is_required && !$field->default)}}
    @endif
    @if($field->is_array)
        {{html()->hidden('arrays[]', $field->name)}}
    @endif
    <small class="form-text text-muted">{{$field->description}}</small>
</div>