<?php

namespace MindOfMicah\ArtisanGUI;

use Illuminate\Support\Arr;
use MindOfMicah\ArtisanGUI\Contracts\CommandOptionable;
use RuntimeException;

class OptionValueCache
{
    private $globals = [];
    private $command_options = [];

    public function prepare(array $configuration, array $globals = [])
    {
        foreach ($globals as $field => $values) {
            $this->globals[$field] = $this->normalizeValues($values);
        }

        foreach ($configuration as $command => $fields) {
            foreach ($fields as $field => $values) {
                $this->command_options[$command][$field] = $this->normalizeValues($values);
            }
        }
    }

    /**
     * @param mixed $configuration
     *
     * @return bool
     */
    private function valueConfigIsAcceptable($configuration): bool
    {
        return is_array($configuration) || $this->isOptionClass($configuration);
    }

    private function isOptionClass($value): bool
    {
        return class_exists($value) && class_implements($value)[CommandOptionable::class] ?? false;
    }


    /**
     * @param array|CommandOptionable $values
     *
     * @return array
     */
    private function normalizeValues($values): array
    {
        if (!$this->valueConfigIsAcceptable($values)) {
            throw new RuntimeException('Config Error: should be an array or instance of ' . CommandOptionable::class);
        }

        $ret = [];
        foreach (Arr::wrap($values) as $key => $current) {
            if (is_array($current)) {
                $ret[$key] = $this->normalizeValues($current);
            } elseif ($this->isOptionClass($current)) {
                foreach (resolve($current)->values() as $index => $value) {
                    $ret[$index] = $value;
                }
            } else {
                if (is_int($key)) {
                    $key = $current;
                }
                $ret[$key] = $current;
            }
        }

        return $ret;
    }

    /**
     * Fetch the possible values for a command/option combination
     *
     * @param string $command_name
     * @param string $option
     *
     * @return array
     */
    public function get(string $command_name, string $option): array
    {
        return !empty($this->command_options[$command_name][$option])
            ? $this->command_options[$command_name][$option]
            : $this->globals[$option] ?? [];
    }
}