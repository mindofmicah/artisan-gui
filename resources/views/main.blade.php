<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Artisan GUI</title>
    <link rel="stylesheet" href="{{asset(mix('app.css','vendor/artisan-gui'))}}">

    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        html, body {
            height: 100%;
            min-height: 100%;
        }
    </style>
</head>
<body>
<div style="display:flex; flex-direction:column; min-height:100vh;" id="app">
    <header style="padding:0 20px; width:auto;" class="border-bottom shadow">
        <div id="options" style="display:flex; justify-content:space-between; align-items:center;">
            <h1 class="h3" style="margin:15px 0;">Artisan Commands</h1>
            <div class="input-group input-group-sm" style="width:40%;">
                <div class="input-group-prepend">
                    <span class="input-group-text">Normal</span>
                </div>
                <input v-model="verbosity" type="range" value="1" min="0" max="4" step="1" class="form-control form-control-range" id="formControlRange">
                <div class="input-group-append">
                    <span class="input-group-text">Verbose</span>
                </div>
            </div>
        </div>
    </header>
    <div class="flex-1" style="overflow-y:auto;">
        <div class="container shadow">
            <div class="accordion" id="accordionExample">
                @foreach($commands as $namespace => $command_list)
                    <div class="card">
                        <div class="card-header" id="heading{{$loop->iteration}}">
                            <h2 class="mb-0">
                                <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{$loop->iteration}}" aria-expanded="true" aria-controls="collapse{{$loop->iteration}}">
                                    {{$namespace}}
                                </button>
                            </h2>
                        </div>
                        <div id="collapse{{$loop->iteration}}" class="collapse {{$loop->first ? ' show' : ''}}" aria-labelledby="heading{{$loop->iteration}}" data-parent="#accordionExample">
                            <div class="card-body">
                                @foreach($command_list as $command)
                                    <form @submit.prevent="submit" class="card card-body" action="{{route('artisan-gui.run', [$command->name])}}" method="post">
                                        {{csrf_field()}}
                                        <div style="display:flex; justify-contents: space-between; align-items:center;" class="flex-col sm:flex-row">

                                            <div class="sm:flex-1">
                                                <strong>{{$command->name}}</strong>
                                                <span class="ml-2 text-muted">{{$command->description}}</span>
                                            </div>
                                            <div style="width:auto;">
                                                @if($command->has_options)
                                                    <button type="button" class="btn btn-light btn-sm" data-toggle="collapse" data-target="#collapse{{$loop->iteration}}-{{$loop->parent->iteration}}">
                                                        <i class="mdi mdi-settings"></i> Add arguments/options
                                                    </button>
                                                @else
                                                    <button class="btn btn-primary btn-sm" type="submit">
                                                        <i class="mdi mdi-run"></i>Run
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="collapse" id="collapse{{$loop->iteration}}-{{$loop->parent->iteration}}">
                                            <div class="flex flex-wrap md:-mx-2">
                                                @foreach($command->arguments as $field)
                                                    @include('artisan-gui::_partials.input', ['key'=>'arguments'])
                                                @endforeach
                                                @foreach($command->options as $field)
                                                    @include('artisan-gui::_partials.input', ['key'=>'options'])
                                                @endforeach
                                            </div>
                                            @foreach($command->switches as $switch)
                                                <div>
                                                    <label class="form-check-label" for="{{$switch->label}}-{{$command->name}}">
                                                        <input type="checkbox" name="switches[{{$switch->name}}]" class="form-check-input" id="{{$switch->label}}-{{$command->name}}"/>
                                                        {{$switch->name}}
                                                        <small class="form-text text-muted">{{$switch->description}}</small>
                                                    </label>
                                                </div>
                                            @endforeach
                                            <div class="text-right">
                                                <button class="btn btn-primary btn-sm" type="submit">
                                                    <i class="mdi mdi-run"></i>Run
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <modal :show="modal_open">
        <div class="flex justify-between border-b items-center mb-1 pb-1" v-bind:class="{'bg-red-500 text-white': errors.length}">
            <h5>Command Output</h5>
            <button type="button" @click="modal_open=false">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div>
            <div v-if="errors.length">
                <p v-for="error in errors" class="text-red-500">@{{error}}</p>
            </div>
            <div v-else-if="output" v-html="output"></div>
            <div v-else class="d-flex justify-content-center">
                <div class="spinner-grow text-dark"></div>
            </div>
        </div>
        <div class="text-right">
            <button type="button" class="btn btn-secondary" @click="modal_open=false">Close</button>
        </div>

    </modal>
    <portal-target name="modals"></portal-target>
</div>
<script src="{{asset(mix('app.js', 'vendor/artisan-gui'))}}"></script>
</body>
</html>
