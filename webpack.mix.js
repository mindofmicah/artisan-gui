const mix = require('laravel-mix');
const webpack = require('webpack');
require('laravel-mix-purgecss');

mix.setPublicPath('public')
    .autoload({
        jquery:['$','window.jQuery', 'jQuery'],
        'popper.js/dist/popper.js':['Popper'],
    })
    .js('resources/js/app.js', 'public')
    .copyDirectory('node_modules/zondicons', 'resources/svg')
    .sass('resources/sass/app.scss', 'public')
    .postCss('resources/css/app.css', 'public', [
        require('postcss-import'),
        require('tailwindcss')('./tailwind.config.js')
    ])
    .sourceMaps()
    .version()
    .copy('public', '../test/public/vendor/artisan-gui')
    ;
